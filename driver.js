// Declare app level module which depends on views, and components
var app = angular.module('theDriver', ['ngMaterial']).controller('mainController', function($scope, $http, $sce, $compile) {

    $scope.showLevel = [false, false, false, false, false, false]
 //  ___  _   ___ ___   _  _   ___   _____ ___   _ _____ ___ ___  _  _
 // | _ \/_\ / __| __| | \| | /_\ \ / /_ _/ __| /_\_   _|_ _/ _ \| \| |
 // |  _/ _ \ (_ | _|  | .` |/ _ \ V / | | (_ |/ _ \| |  | | (_) | .` |
 // |_|/_/ \_\___|___| |_|\_/_/ \_\_/ |___\___/_/ \_\_| |___\___/|_|\_|

    $scope.goto = function(page) {
        $http.get(page).then(function successCallback(response) {
            $scope.currentpage = $sce.trustAsHtml(response.data)
        }, function errorCallback(response) {
            alert("ERROR: something wrong with page request.");
            console.log(response);
        });
    }
    // default page
    $scope.goto("pages/search.html")
    $scope.currentNavItem = "searchpage"

    //  ___ _____ ___ _   _  ___ _____ _   _ ___ ___ ___
    // / __|_   _| _ \ | | |/ __|_   _| | | | _ \ __/ __|
    // \__ \ | | |   / |_| | (__  | | | |_| |   / _|\__ \
    // |___/ |_| |_|_\\___/ \___| |_|  \___/|_|_\___|___/

    /**
     * Create and return a new trieNode object
     * @param  {char} letter the letter the trieNode corresponds to
     * @return {object}      the trieNode
     */
    var newTrieNode = function(letter, word) {
        return {
            "map": new Map(), //the next search nodes; KV pair letter of node -> trieNode Object
            "using": [ word ], //which words are using this trieNode
            "letter": letter, //which letter this trieNode is for
            "results": null //if this node ends a word, holds possible results of word
        }
    }

    //  ___ _  _ ___ _____ ___   _   _    ___ ____  _ _____ ___ ___  _  _
    // |_ _| \| |_ _|_   _|_ _| /_\ | |  |_ _|_  / /_\_   _|_ _/ _ \| \| |
    //  | || .` || |  | |  | | / _ \| |__ | | / / / _ \| |  | | (_) | .` |
    // |___|_|\_|___| |_| |___/_/ \_\____|___/___/_/ \_\_| |___\___/|_|\_|

    $scope.trie = {
        "map": new Map(),
        "using": [] //ends up with all words in dictionary
    };
    // array of spells by level: spell name to spell object
    $scope.spells = [];

    /**
     * puts a single word into the trie
     * @param  {string} word        the word to add
     * @param  {string} results the roll results for this word
     */
    var put = function(word, results) {
        var current = $scope.trie;
        var character = 0; //which character of the word we're at
        while (current.map.has(word.charAt(character))) {
            // traverse as far as we can along already made nodes
            console.log(word.charAt(character));
            current.using.push(word)
            current = current.map.get(word.charAt(character)); //next node
            character++; //next character in word
            if (character === word.length) {
                // we've reached the end of the word -- already exists in some path
                if (current.results) {
                    alert("duplicate definitions in data dictionary");
                    return;
                }
                console.log(word, "has been added to an existing node");
                current.results = results;
                return;
            }
        }
        current.using.push(word)
        // we have traversed to a point where current is missing the next letter in our word
        // add new branch of trie
        while (character < word.length) {
            var curChar = word.charAt(character);
            console.log("+", curChar);
            current.map.set(curChar, newTrieNode(curChar, word));
            current = current.map.get(curChar); //next
            character++
        }
        console.log("finished adding word", word);
        current.results = results;
    }

    /**
     * Processes the data into the trie, and builds $scope.spells
     * @param  {object} data the data to process
     */
    var processdata = function(data) {
        data.forEach(function(word) {
            // put into trie
            put(word.name.toLowerCase(), word)
            // get level
            level = word.notes.charAt(7);
            if (!$scope.spells[level]) $scope.spells[level] = [];
            console.log("level", level);
            $scope.spells[level].push(word)
        });
        console.log("finished processing data");
        console.log("trie:", $scope.trie);
        console.log("spells:", $scope.spells);
        // console.log($scope.trie);
    }

    // retrives the data on page load, processes data
    $http.get("data.json").then(function successCallback(response) {
        console.log("loaded data", response.data);
        var data = response.data
        // console.log(data);
        processdata(response.data.data)
    }, function errorCallback(response) {
        alert("ERROR: data misformatted or not found (see console).");
        console.log(response);
    });



    //  ___ _   _ _  _  ___ _____ ___ ___  _  _   _   _    ___ _______   __
    // | __| | | | \| |/ __|_   _|_ _/ _ \| \| | /_\ | |  |_ _|_   _\ \ / /
    // | _|| |_| | .` | (__  | |  | | (_) | .` |/ _ \| |__ | |  | |  \ V /
    // |_|  \___/|_|\_|\___| |_| |___\___/|_|\_/_/ \_\____|___| |_|   |_|


    // default values for content pane items
    $scope.resultcontent = "";
    $scope.searchnumber = "";
    $scope.suggestions = $scope.trie.using; // all items in dictionary
    $scope.errorsearch = false; //whether this search resulted in any possibilities

    $scope.autocomplete = true; //automatically complete results

    var previousvalidsearch = ""; //the last non-erroreous search
    var previousSearchTextLength = 0; //how long the previous search was

    /**
    * Search for searchtext and searchnumber in the trie.
    * Sets resultcontent to the result of the search
    */
    $scope.search = function() {
        // if there is no search term
        if (!$scope.searchtext || $scope.searchtext.length === 0) {
            $scope.suggestions = $scope.autocomplete ? [] : $scope.trie.using;
            // $scope.suggestions = $scope.trie.using; // all items in dictionary
            $scope.suggestions = []; // don't display when there's no results
            $scope.resultcontent = null;
            previousSearchTextLength = 0;
            previousvalidsearch = ""
            return;
        }
        // backspace when autocomplete is on deletes entire search
        if ($scope.autocomplete && previousSearchTextLength > $scope.searchtext.length) {
            $scope.searchtext = "";
            previousSearchTextLength = 0;
            previousvalidsearch = ""
            $scope.search();
            return
        }

        // search trie
        var word = $scope.searchtext.toLowerCase();

        var current = $scope.trie;
        var character = 0;
        while (current.map.has(word.charAt(character))) {
            current = current.map.get(word.charAt(character)); //next node
            character++
            if (character === word.length) {
                // we've reached the end of the word and it exists in some path
                // console.log("reahed end of word", current);

                if ($scope.autocomplete && previousSearchTextLength < $scope.searchtext.length && !current.results) {
                    // continue searching down if there's only one path to a word,
                    // but only if we added new letters (not backspaces)

                    // console.log("autocompleting");
                    // console.log(current);
                    while(current.map.size === 1 && !current.results) {
                        // get next
                        current = current.map.get(current.map.keys().next().value);

                        // console.log("+", current.letter);
                        $scope.searchtext += current.letter;
                        // console.log(current.map.keys().next());
                        // current = current.map.get(Object.keys(current.map[0]));
                    }

                    //end of autocomplete
                    // if there is a result, go to number input box
                    if (current.results) {
                        var textinput = document.getElementById("searchnumber");
                        // textinput.select(); // would select the contents
                        textinput.focus();
                    }
                }
                $scope.suggestions = current.using; // TODO make better
                $scope.resultcontent = current.results;
                $scope.errorsearch = false;
                previousSearchTextLength = $scope.searchtext.length;
                previousvalidsearch = $scope.searchtext;

                return;
            }
        }
        // failed search
        if ($scope.autocomplete) { //if autocomplete is on, we don't allow searches to fail (revert to previous valid search)
            $scope.searchtext = previousvalidsearch;
            $scope.search();
            return;
        }
        // clear since we have no data for this point
        previousSearchTextLength = $scope.searchtext.length;
        $scope.errorsearch = true;
        $scope.resultcontent = null;
        // $scope.suggestions = ""; //comment this line to change behavior: misspelling a word shows last suggestions
    } //end search

    /**
     * Set the current searchtext to the given word
     * @param  {string} word the word to set the searchtext to
     */
    $scope.setCurrentSearchTerm = function(word) {
        console.log("setting search to", word);
        $scope.searchtext = word;
        var tmpautocomplete = $scope.autocomplete;
        $scope.autocomplete = false;
        $scope.search();
        $scope.autocomplete = tmpautocomplete;
        $scope.goto("pages/search.html")
        $scope.currentNavItem = "searchpage"
        // timeout for 100s so that page has time to load
        if ($scope.searchtext) setTimeout(function() {document.getElementById("searchnumber").focus()}, 100)
    }

    /**
     * Return the coloring of the text search field according to whether the search was successful or not
     */
    $scope.searchcolor = function() {
        return {
            "color": $scope.errorsearch ? "red" : "black"
        }
    }

    /**
     * Style the autocomplete button according to the status of autocomplete
     * @param {boolean} bool whether or not the button has been toggled
     */
    $scope.buttonstyle = function(bool) {
        return {
            "background": bool ? "rgb(201, 201, 201)" : "rgb(240, 240, 240)"
        }
    }

    /**
     * Return whether the result should be hidden.
     * @param  {string} interval properly formatted interval. Returns false if $scope.searchnumber is inside of this interval
     * @return {boolean}         whether or not the searchnumber falls within the interval
     */
    $scope.isHidden = function(interval) {
        if (!$scope.searchnumber) return false;
        // parse interval
        if (interval.indexOf('+') > -1) {
            // non-ending range
            return parseInt(interval.substring(0,interval.length-1)) > parseInt($scope.searchnumber);
        } else if (interval.indexOf("-") === -1) { //only a single position
            return parseInt($scope.searchnumber) !== parseInt(interval)
        } else {
            // extract low and high
            split = interval.split("-");
            // console.log(split);
            return parseInt($scope.searchnumber) < parseInt(split[0]) || parseInt($scope.searchnumber) > parseInt(split[1]);
        }
    }

    /**
     * Compares two intervals and returns the larger
     * @param  {string} int1 interval 1
     * @param  {string} int2 interval 2
     * @return {integer} 1 if int1 > int2, 0 if the same, -1 if int2 > int1
     */
    $scope.compareIntervals = function(int1, int2) {
        // parse int1, int2
        var int1p, int2p;

        if (int1.indexOf('+') > -1) {
            // non-ending range
            int1p = parseInt(int1.substring(0,int1.length-1))
        } else if (int1.indexOf("-") === -1) { //only a single position
            int1p = parseInt(int1)
        } else {
            // extract low
            int1p = int1.split("-")[0];
        }

        if (int2.indexOf('+') > -1) {
            // non-ending range
            int2p = parseInt(int2.substring(0,int2.length-1))
        } else if (int2.indexOf("-") === -1) { //only a single position
            int2p = parseInt(int2)
        } else {
            // extract low
            int2p = int2.split("-")[0];
        }


        return int1p - int2p;
    }

    /**
     * Handle the number input interface element behavior
     */
    $scope.numberchange = function() {
        //if it is a backspace on a blank field
        if (event.keyCode === 8 && $scope.searchnumber.length === 0) {
            var textinput = document.getElementById("searchtext");
            // textinput.select(); // would select the contents
            textinput.focus();
        }
    }
});
