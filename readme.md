# dcc_triesearch
## Purpose
1. To provide a quick and intuitive way of searching through the DCC spell lists. The two provided fields - one for spell name, one for roll - make it easy to find exactly the result of your spellcasting without flipping through the book.
2. Because computer science is fun! This was an excellent excuse to implement a trie for the purpose of autocomplete. The code of this project is highly extensible for parsing properly formatted json of word-definition pairs.

## Usage
1. Clone the repo to some folder
2. Start server.sh (`sh server.sh`) to start a simple http server -- this project uses Angularjs' $http service which means it won't work just opening the HTML in your browser.
3. Navigate your web browser to localhost:31415 to access the program.
4. Type the spell name you want into the left text-input field. If quick-search is on it should autofill most of what you're typing, making it easier if you only have one hand with which to type (as I find commonly as a DM).
5. Type the roll result into the second text input field.
6. Quick-search can be toggled with the button of the same name. Quick-search has the following features:
    * After typing a character, completes the word to the farthest extent it can without eliminating any possible intended results.
    * Deletes the entire search when you backspace.
    * Disallows entering characters which would not result in a valid search -- mash the keyboard all you want, it'll get you to a valid result!
7. You can also click on the suggested results beneath the search bar to complete your search without more typing.

## Notes

* Spell data was sourced from Purple Sorcerer Games' [Grimoire tool](https://purplesorcerer.com/grimoire.php). To speed conversion, the result matrix can be parsed into json using the  following regex: `(\d+(?:-\d*)?\+?): (.*)` using the replace string `{"$1": "$2"},`. Use [http://regexr.com/](http://regexr.com/) for this step.